import React, { useEffect, useState } from 'react';
import './App.css'
 
function Counter() {
 const [count, setCount] = useState(0);

 useEffect(() => {
     document.title = `Total is ${count}`
 })

    return (
    <div>
         <p>Total Count is: {count} </p>
         <button className='btn1' onClick={() => setCount(count + 1)}>Increment</button>
         <button className='btn2' onClick={() => setCount(count - 1)}>Decrement</button>
    </div>
   );
 }

 export default Counter



